# Mink List
- Dieses Repository enthält das "Mobile Applications"-Semesterabschlussprojekt im Rahmen meiner Ausbildung bei CODERS.BAY Vienna.
- Mit dieser Android-App kann man sich mit einem User anmelden, Einkaufslisten erstellen und diese mit anderen Teilen zum gemeinsamen Einsehen und Bearbeiten.

## Frontend
- Das Frontend ist im Kotlin-Framework Jetpack Compose entwickelt.
- Es folgt dem Entwurfsmuster MVVM

## Backend
- Das Backend ist in Kotlin geschrieben.

### Datenbank
Das Projekt verwendet zwei Datenbanken:

- Cloud-basierte Datenbank: Firebase/Firestore
  - Um Daten aller Nutzer auf einem Cloud-Server zu speichern.
- Lokale Datenbank: JetBrains/Exposed
  - Um Daten des angemeldeten Users auf dem Gerät zu speichern.

### Anmeldung
- Die Registrierung und Anmeldung habe ich selbst geschrieben.
- Sicherheit ist 2/10 also Warum? -Um es Mal gemacht zu haben. 
- Passwörter werden mit MD5 gehasht.

## Screenshots
- Farben passen sich an die Geräteeinstellungen des Nutzers an. Hier: darkmode/lila-Theme
![MinkList Screenshots](MinkList_Screenshots.png)
