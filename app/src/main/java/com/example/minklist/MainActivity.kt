package com.example.minklist

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.minklist.data.local.clearTable
import com.example.minklist.data.local.createNewTable
import com.example.minklist.data.local.schema.Users
import com.example.minklist.ui.drawer.DrawerViewModel
import com.example.minklist.ui.drawer.Drawer
import com.example.minklist.ui.options.EmptyImplementation
import com.example.minklist.ui.options.LoadingScreen
import com.example.minklist.ui.options.login.Login
import com.example.minklist.ui.options.OptionsViewModel
import com.example.minklist.ui.options.login.RegisterName
import com.example.minklist.ui.options.login.RegisterPassword
import com.example.minklist.ui.theme.MinkListTheme
import com.google.firebase.Firebase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.firestore
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val dbRemote = Firebase.firestore
        val dblocal =
            Database.connect("jdbc:h2:$filesDir/db")    // data/data/com.example.minklist/files/db.db

//        clearTable()

        var currentUser = ""
        transaction {
            Users.select(Users.name).where { Users.id eq 1 }.forEach {
                currentUser = it[Users.name]
            }
        }

        setContent {
            MinkListTheme {

                val navController = rememberNavController()
                NavHost(
                    navController, startDestination = if (currentUser == "") {
                        "login"
                    } else {
                        "drawer"
                    }
                ) {
                    composable(route = "drawer") {
                        Drawer(
                            vm = DrawerViewModel(), navController, dbRemote, dblocal
                        )
                    }

                    composable(route = "login") {
                        Login(vm = OptionsViewModel(), navController)
                    }

                    composable(
                        route = "emptyImplementation/{goBackTo}",
                        arguments = listOf(navArgument("goBackTo") { defaultValue = "drawer" })
                    ) { backStackEntry ->
                        EmptyImplementation(
                            navController,
                            backStackEntry.arguments?.getString("goBackTo")
                        )
                    }

                    composable(route = "register") {
                        RegisterName(vm = OptionsViewModel(), navController)
                    }

                    composable(route = "registerPassword/{username}",
                        arguments = listOf(navArgument("username") { defaultValue = "" })
                    ) { backStackEntry ->
                        RegisterPassword(vm = OptionsViewModel(), navController,
                            backStackEntry.arguments?.getString("username"), dbRemote)
                    }

                    composable(route = "load"){
                        LoadingScreen(navController)
                    }
                }
            }
        }
    }
}