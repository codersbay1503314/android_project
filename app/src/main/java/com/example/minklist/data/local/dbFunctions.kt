package com.example.minklist.data.local

import com.example.minklist.data.local.dao.User
import com.example.minklist.data.local.schema.Shoppinglists
import com.example.minklist.data.local.schema.Users
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction


fun createNewTable() {
    transaction {
        SchemaUtils.create(Users)   // Creates new table "Users"
        SchemaUtils.create(Shoppinglists)

        User.new {                // Creates new User in table "Users"
            name = ""
            password = ""
        }
    }
    if (getUserCount() > 1)
        deleteAllOtherUsersFromTable()
}

fun clearTable() {
    transaction {
        SchemaUtils.drop(Shoppinglists)
        SchemaUtils.drop(Users)
        SchemaUtils.create(Users)   // Creates new table "Users"
        SchemaUtils.create(Shoppinglists)

        User.new {                // Creates new User in table "Users"
            name = ""
            password = ""
        }
    }
}

fun getUserCount(): Int {
    return transaction {
        Users.selectAll().count()
    }.toInt()
}

fun deleteAllOtherUsersFromTable() {
    val count = getUserCount()
    if (count > 1) {
        var idToDelete = 2
        while (idToDelete <= count) {
            transaction { Users.deleteWhere { Users.name eq "" } }
            idToDelete++
        }
    }
}


fun getUsername(): String {
    var name = ""
    transaction {
        Users.selectAll().where { Users.id eq 1 }.forEach {
            name = it[Users.name].toString()
        }
    }
    return name
}

fun getFirstList(): String {
    var code = ""
    transaction {
        Shoppinglists.selectAll().where { Shoppinglists.id eq 1 }.forEach {
            code = it[Shoppinglists.listCode]
        }
    }
    return code
}