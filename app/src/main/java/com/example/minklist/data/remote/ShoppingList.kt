package com.example.minklist.data.remote

import kotlin.reflect.KProperty

class ShoppingList(
    var listcode: String = "XXXX",
    var listname: String = "My List \uD83D\uDC39",
    var listitems: MutableList<String> = mutableListOf("Milch", "Eier", "Salz"),
    var listusers: MutableList<String> = mutableListOf("Löwi")
) {

    fun changeName(newName: String) {
        listname = newName
    }

    fun addItem(item: String) {
        listitems.add(item)
        println(listitems)
    }

    fun deleteItem(index: Int) {
        listitems.removeAt(index)
        println(listitems)
    }

    operator fun getValue(nothing: Nothing?, property: KProperty<*>): ShoppingList {
        return ShoppingList()
    }
}