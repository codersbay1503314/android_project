package com.example.minklist.data.local.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object Shoppinglists : IntIdTable() {
    val listCode = varchar("listcode", 4)
    val listName = varchar("listname", 255)
    val listItems = array<String>("items")
    val listUser = array<String>("user")
}