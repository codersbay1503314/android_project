package com.example.minklist.data.remote

data class LoginUser(
    var name: String,
    var pass: String
) {

    var username: String
    var password: String

    init {
        this.username = name
        this.password = pass
    }

    fun setusername(name: String) {
        username = name
    }

    fun getname(): String {
        return username
    }


    fun setpassword(pass: String) {
        password = pass
    }

    fun getpassword(): String {
        return password
    }
}