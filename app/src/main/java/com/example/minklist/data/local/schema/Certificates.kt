package com.example.minklist.data.local.schema

import org.jetbrains.exposed.sql.Table

object Certificates : Table() {
    val user = reference("user", Users)
}