package com.example.minklist.data.local.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object Users : IntIdTable() {
    val name = varchar("name", 255)
    val password = varchar("password",255)
}