package com.example.minklist.data.local.dao

import com.example.minklist.data.local.schema.Shoppinglists
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Shoppinglist(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Shoppinglist>(Shoppinglists)
    var listCode by Shoppinglists.listCode
    var listName by Shoppinglists.listName
    var listItems by Shoppinglists.listItems
    var listUser by Shoppinglists.listUser
}