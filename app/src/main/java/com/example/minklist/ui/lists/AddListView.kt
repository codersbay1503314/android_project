package com.example.minklist.ui.lists

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.minklist.ui.drawer.TopBarView
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun AddListView(
    db: FirebaseFirestore,
    navigationIconOnClick: () -> Unit, navAppController: NavHostController
) {
    val navController = rememberNavController()

    TopBarView(
        title = "add list",
        navigationIconOnClick = navigationIconOnClick,
        actions = {}
    ) {
        NavHost(
            navController,
            startDestination = "joinList"
        ) {
            composable(route = "joinList") {
                JoinListView(
                    db,
                    vm = ListViewModel(),
                    navController,
                    navAppController
                )
            }
            composable(route = "createList") {
                CreateListView(
                    db,
                    vm = ListViewModel(),
                    navController,
                    navAppController
                )
            }
        }
    }
}