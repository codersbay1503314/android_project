package com.example.minklist.ui.lists

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Check
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.Edit
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun ListOptionView(
    db: FirebaseFirestore,
    vm: ListViewModel,
    listCode: String?,
    navController: NavHostController,
    appNavController:NavHostController
) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primaryContainer),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Column(
            modifier = Modifier
                .padding(10.dp)
                .background(MaterialTheme.colorScheme.background, RoundedCornerShape(25.dp))
                .padding(20.dp, 10.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Column(
                Modifier
                    .fillMaxWidth()
                    .padding(3.dp)
            ) {

                Row(
                    Modifier
                        .height(45.dp)
                        .padding(4.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Row {
                        Text(
                            text = "list name: ",
                            color = MaterialTheme.colorScheme.primary
                        )
                    }
                    Spacer(modifier = Modifier.size(5.dp))
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = vm.getListName(listCode.toString()),
                            style = TextStyle(
                                color = MaterialTheme.colorScheme.primary,
                                fontSize = 20.sp
                            )
                        )
                        IconButton(onClick = { navController.navigate("emptyImplementation/addList") }) {
                            Icon(
                                imageVector = Icons.Outlined.Edit,
                                contentDescription = "Edit list name",
                                modifier = Modifier.size(20.dp),
                                tint = MaterialTheme.colorScheme.primary
                            )
                        }
                    }
                }

                Row(
                    Modifier
                        .height(45.dp)
                        .padding(4.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Row {
                        Text(
                            text = "list code:  ",
                            color = MaterialTheme.colorScheme.primary
                        )
                    }
                    Spacer(modifier = Modifier.size(5.dp))
                    Row {
                        Text(
                            text = listCode.toString(),
                            style = TextStyle(
                                color = MaterialTheme.colorScheme.primary,
                                fontSize = 20.sp
                            )
                        )

                    }
                }
            }

            Spacer(modifier = Modifier.size(15.dp))

            ExtendedFloatingActionButton(onClick = {
                vm.deleteMarkedItems(
                    listCode.toString(),
                    db
                )
                navController.navigate("list/$listCode")
            }) {
                Row {
                    Icon(
                        imageVector = Icons.Outlined.Delete,
                        contentDescription = "Delete marked items",
                        modifier = Modifier.size(23.dp)
                    )
                    Icon(
                        imageVector = Icons.Outlined.Check,
                        contentDescription = "Delete marked items",
                        modifier = Modifier.size(23.dp)
                    )
                    Spacer(modifier = Modifier.size(10.dp))
                    Text(
                        text = "Delete marked items", fontSize = (15.sp)
                    )
                }
            }

            Spacer(modifier = Modifier.size(30.dp))

            Row(
                Modifier.fillMaxWidth(0.8f),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutlinedButton(
                    onClick = {
                        vm.leaveList(listCode.toString(), db)
                        appNavController.navigate("load")
                    },
                    border = BorderStroke(1.dp, color =  Color(0xffd30000))
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Icon(
                            imageVector = Icons.Outlined.Delete,
                            contentDescription = "Delete List",
                            modifier = Modifier.size(23.dp),
                            tint = Color(0xffd30000)
                        )
                        Text(text = "leave list", color = Color(0xffd30000))
                    }
                }

                Spacer(modifier = Modifier.size(10.dp))

                FilledTonalButton(onClick = { navController.navigate("list/$listCode") }) {
                    Text(text = "go back", color = MaterialTheme.colorScheme.primary)
                }
            }
            Spacer(modifier = Modifier.size(10.dp))
        }
    }
}


