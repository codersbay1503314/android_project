package com.example.minklist.ui.lists

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.example.minklist.data.local.getUsername
import com.example.minklist.data.local.schema.Shoppinglists
import com.example.minklist.data.local.schema.Users
import com.google.firebase.firestore.FirebaseFirestore
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class ListViewModel : ViewModel() {
    var state by mutableStateOf(ListState())
        private set

    fun getListName(code: String): String {
        var name = ""
        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.listCode eq code }.forEach {
                name = it[Shoppinglists.listName]
            }
        }
        return name
    }

    fun getListItems(listCode: String): List<String> {
        var list: List<String> = listOf()
        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.listCode eq listCode }.forEach {
                list = it[Shoppinglists.listItems]
            }
        }
        return list
    }

    fun writeItem(item: String) {
        if (item.length <= 30) {
            state = state.copy(item = item)
        }
    }

    fun strikeThrough(str: String): Boolean {
        return str.contains("//---//")
    }

    fun addItem(listCode: String, db: FirebaseFirestore) {
        val list = db.collection("shoppinglists")
        var n: String = ""
        var i: List<String> = mutableListOf()
        var u: List<String> = mutableListOf()


        if (state.item != "" && state.item.length <= 28)
            transaction {
                Shoppinglists.selectAll().where { Shoppinglists.listCode eq listCode }.forEach {
                    n = it[Shoppinglists.listName].toString()
                    i = it[Shoppinglists.listItems]
                    u = it[Shoppinglists.listUser]
                }
            }
        val newItems: List<String> = i + state.item

        data class SList(
            var code: String = listCode,
            var name: String = n,
            var items: List<String> = newItems,
            var user: List<String> = u
        )

        list.document(listCode)
            .set(SList())
            .addOnSuccessListener {

                transaction {
                    Shoppinglists.update({ Shoppinglists.listCode eq listCode }) {
                        it[listItems] = newItems
                    }
                }
            }
        state = state.copy(item = "")   // clear textfield
    }

    fun checkItem(c: String, index: Int, db: FirebaseFirestore) {
        val listRemote = db.collection("shoppinglists")
        val list = getListItems(c).toMutableList()

        if (list[index].contains("//---//")) {
            list[index] = list[index].dropLast(7)
        } else {
            list[index] = list[index] + "//---//"
        }

        transaction {
            Shoppinglists.update({ Shoppinglists.listCode eq c }) {
                it[listItems] = list
            }
        }

        var n: String = ""
        var u: List<String> = mutableListOf()

        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.listCode eq c }.forEach {
                n = it[Shoppinglists.listName].toString()
                u = it[Shoppinglists.listUser]
            }
        }

        data class SList(
            var code: String = c,
            var name: String = n,
            var items: List<String> = list,
            var user: List<String> = u
        )

        listRemote.document(c)
            .set(SList())
    }


    // CreateListView
    fun writeListName(input: String) {
        state = state.copy(nameField = input)
    }

    fun writeListCode(input: String) {
        if (input.isDigitsOnly() && input.length <= 4) {
            state = state.copy(codeField = input)
        }
    }

    fun isCodeAvailable(db: FirebaseFirestore){
        val list = db.collection("shoppinglists").document(state.codeField)
        list.get().addOnSuccessListener { document ->
            if (document.data != null){
                state.listCodeAlert = true
                state.listCodeSuccess = false
                writeListCode("")
            } else {
                state.listCodeAlert = false
                state.listCodeSuccess = true
                createList(db)
            }
        }
    }

    private fun createList(db: FirebaseFirestore) {
        sendListToRemoteDB(state.codeField, state.nameField, listOf(), listOf(getUsername()), db)

        val user = db.collection("user").document(getUsername())
        var pw = ""
        var lists: List<String> = listOf()
        user.get()
            .addOnSuccessListener { document ->
                pw = document["password"].toString()
                lists = document["lists"] as List<String>
                lists += state.codeField

                addListToLocalDB(state.codeField, state.nameField, listOf(), listOf(getUsername()))
                sendUpdatedUser(getUsername(), pw, lists, db)
            }
    }

    fun findList(db: FirebaseFirestore, navController: NavController) {
        val list = db.collection("shoppinglists").document(state.codeField)
        list.get().addOnSuccessListener { document ->
            if (document.data == null) {
                state.listCodeAlert = true
                state.listCodeSuccess = false
                writeListCode("")
            } else {
                state.listCodeAlert = false
                state.listCodeSuccess = true
                joinList(db, navController)
            }
        }
    }

    // add list to user
    fun joinList(db: FirebaseFirestore, navController: NavController) {
        val user = db.collection("user").document(getUsername())
        var pw = ""
        var lists: List<String> = listOf()
        user.get()
            .addOnSuccessListener { document ->
                pw = document["password"].toString()
                lists = document["lists"] as List<String>
                lists = lists + state.codeField

                // todo: aktualisiere liste in firebase
                // -> get list from firebase
                val list = db.collection("shoppinglists").document(state.codeField)

                var name: String = ""
                var items: List<String> = listOf()
                var user: List<String> = listOf()

                list.get()
                    .addOnSuccessListener { document ->
                        name = document["name"].toString()
                        items = document["items"] as List<String>
                        user = document["user"] as List<String>

                        // -> add this user to userList
                        user = user + getUsername()

                        // -> add list to localDB
                        addListToLocalDB(
                            state.codeField,
                            name,
                            items,
                            user
                        )
                        // -> shoppinglists.set(liste mit veränderten usern)
                        sendListToRemoteDB(state.codeField, name, items, user, db)
                        sendUpdatedUser(getUsername(), pw, lists, db)
                    }
            }
    }

    private fun sendUpdatedUser(
        n: String,
        p: String,
        l: List<String>,
        db: FirebaseFirestore
    ) {
        data class U(
            var name: String = n,
            var password: String = p,
            val lists: List<String> = l
        )

        val user = db.collection("user").document(n)
        user.set(U())
        writeListCode("")
    }

    private fun addListToLocalDB(
        code: String,
        name: String,
        items: List<String>,
        user: List<String>
    ) {
        transaction {
            Shoppinglists.insert {
                it[listCode] = code
                it[listName] = name
                it[listItems] = items
                it[listUser] = user
            }
        }
    }

    private fun sendListToRemoteDB(
        c: String,
        n: String,
        i: List<String>,
        u: List<String>,
        db: FirebaseFirestore
    ) {
        data class NewList(
            var code: String = c,
            var name: String = n,
            var items: List<String> = i,
            var user: List<String> = u
        )

        val shoppinglists = db.collection("shoppinglists").document(c)
        shoppinglists.set(
            NewList()
        )
    }

    fun deleteMarkedItems(listCode: String, db: FirebaseFirestore) {
        var code: String = listCode
        var name: String = ""
        var items: List<String> = listOf()
        var user: List<String> = listOf()

        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.listCode eq listCode }.forEach {
                name = it[Shoppinglists.listName]
                items = it[Shoppinglists.listItems]
                user = it[Shoppinglists.listUser]
            }
        }
        val newList: MutableList<String> = mutableListOf()
        for (item in items) {
            if (!item.contains("//---//")) {
                newList += item
            }
        }
        items = newList

        transaction {
            Shoppinglists.update({ Shoppinglists.listCode eq code }) {
                it[listItems] = items
            }
        }
        sendListToRemoteDB(code, name, items, user, db)
    }

    fun leaveList(listCode: String, db: FirebaseFirestore) {
        var thisUserName = getUsername()
        var code: String = listCode
        var name: String = ""
        var items: List<String> = listOf()
        var user: List<String> = mutableListOf()

        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.listCode eq listCode }.forEach {
                name = it[Shoppinglists.listName]
                items = it[Shoppinglists.listItems]
                user = it[Shoppinglists.listUser]
            }
        }
        if (user.size >= 2) {
            val newUserList: MutableList<String> = mutableListOf()
            for (u in user) {
                if (u != thisUserName) {
                    newUserList += u
                }
            }
            user = newUserList
            sendListToRemoteDB(code, name, items, user, db)
            transaction {
                Shoppinglists.deleteWhere { Shoppinglists.listCode eq code }    // delete localDB
            }
        } else {
            transaction {
                Shoppinglists.deleteWhere { Shoppinglists.listCode eq code }
            }
            val shoppinglists = db.collection("shoppinglists").document(code)
            shoppinglists.delete()
        }
        var password = ""
        var shoppingLists: MutableList<String> = mutableListOf()
        transaction {
            Users.selectAll().where { Users.id eq 1 }.forEach {
                password = it[Users.password]
            }
        }
        transaction {
            Shoppinglists.selectAll().forEach {
                shoppingLists.add(it[Shoppinglists.listCode])
            }
        }
        sendUpdatedUser(thisUserName, password, shoppingLists, db)
    }
}