package com.example.minklist.ui.options

import com.example.minklist.data.local.getUsername
import com.example.minklist.data.remote.LoginUser

data class OptionsState(
    // User options
    var loginUser: LoginUser = LoginUser("", ""),
    var nameChangeField: String = getUsername(),

    // login
    var nameField: String = loginUser.name,
    var passwordField: String = loginUser.pass,
    var usernameAlert: Boolean = false,
    var passwordAlert1: Boolean = false,
    var passwordAlert2: Boolean = false,
    var loginAlert: Boolean = false,
    var loginSuccess: Boolean = false,

    // registration
    var rNameField: String = "",
    var rPassword1Field: String = "",
    var rPassword2Field: String = "",
    var passwortAlert: String = "",
    var isNameTaken: Boolean = false,
    var rUsername:String = "",
    var rPassword: String = "",

    // List options
    var listNameField: String = "My List",
    var listCodes: List<String> = mutableListOf()
)