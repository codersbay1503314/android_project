package com.example.minklist.ui.options

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.minklist.R

@Composable
fun EmptyImplementation(
    navController: NavHostController,
    goBackTo: String?
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFFDFAF3)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.sadminki),
            contentDescription = "sad mink",
            Modifier.size(200.dp)
        )
        Text(text = "Oh no! This feature isn't implemented yet.", color = Color.Black)
        Spacer(Modifier.size(10.dp))
        Text(text = "Let's wait patiently for an update.", color = Color.Black)

        Spacer(Modifier.size(10.dp))

        OutlinedButton(onClick = { navController.navigate(goBackTo.toString()) }) {
            Text(text = "go back", color = Color.DarkGray)
        }
    }
}