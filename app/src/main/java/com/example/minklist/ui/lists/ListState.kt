package com.example.minklist.ui.lists

data class ListState(
    // This String is the item written into the textfield to add a list-item.
    // After submitting it, it will be added to the current ShoppingList.
    val item: String = "",
    val listCode: String = "",
    var listCodeAlert: Boolean = false,
    var listCodeSuccess: Boolean = false,

    //CreateListView
    val nameField: String = "",
    val codeField: String = ""
)
