package com.example.minklist.ui.drawer

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.minklist.data.local.schema.Shoppinglists
import com.google.firebase.firestore.FirebaseFirestore
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class DrawerViewModel : ViewModel() {

    var state by mutableStateOf(DrawerState())
        private set

    var currentList by mutableStateOf(state.curlist)

    fun getListName(code: String): String {
        var listname = ""
        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.listCode eq code }.forEach {
                listname = it[Shoppinglists.listName]
            }
        }
        return listname
    }

    fun setCurlist(newListCode: String) {
        currentList = newListCode
    }

    fun updateList(remoteDb: FirebaseFirestore, localDb: Database, listCode: String) {
        val list = remoteDb.collection("shoppinglists").document(listCode)
        list.get()
            .addOnSuccessListener { document ->
                val listname: String = document["name"].toString()
                val items: List<String> = document["items"] as List<String>
                val user: List<String> = document["user"] as List<String>

                //check for updates
                transaction {
                    Shoppinglists.update({ Shoppinglists.listCode eq listCode }) {
                        it[listName] = listname
                        it[listItems] = items
                        it[listUser] = user
                    }
                }
            }
    }

    fun getAllListsFromUser(
        db: FirebaseFirestore,
        name: String,
        callback: (List<*>) -> Unit
    ) {
        var entries: List<*>
        val user = db.collection("user").document(name)
        user.get()
            .addOnSuccessListener { document ->
                entries = document["lists"] as List<*>
                callback(entries)
            }
            .addOnFailureListener { e ->
                println("Minki traurig weil $e")
            }
    }

    fun getStartDestination(): String {
        var firstList = 0
        transaction {
            firstList = Shoppinglists.selectAll().count().toInt()
        }
        (return if (firstList == 0) {
            "addList"
        } else {
            "list/${getFirstListCode()}"
        })
    }

    private fun getFirstListCode(): String {
        var firstList = ""
        transaction {
            Shoppinglists.selectAll().where { Shoppinglists.id eq 1 }.forEach {
                firstList = it[Shoppinglists.listCode]
            }
        }
        return firstList
    }
}