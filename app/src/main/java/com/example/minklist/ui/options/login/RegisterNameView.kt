package com.example.minklist.ui.options.login

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.minklist.ui.options.OptionsViewModel

@Composable
fun RegisterName(
    vm: OptionsViewModel,
    navController: NavHostController
) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primaryContainer),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .padding(10.dp)
                .background(MaterialTheme.colorScheme.background, RoundedCornerShape(25.dp))
                .padding(20.dp, 10.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "register",
                style = TextStyle(color = MaterialTheme.colorScheme.primary, fontSize = 30.sp)
            )

            Column(
                modifier = Modifier
                    .padding(0.dp, 15.dp, 0.dp, 0.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Column {
                    Text(
                        text = "choose a username",
                        style = TextStyle(color = MaterialTheme.colorScheme.primary)
                    )
                    TextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = vm.state.rNameField,
                        onValueChange = { vm.writeRUsername(it) },
                        placeholder = { Text(text = "CuteMink") },
                        singleLine = true,
                        keyboardActions = KeyboardActions(
                            onDone = {
                                vm.submitName()
                            }
                        )
                    )
                    if (vm.state.usernameAlert) {
                        Text(
                            text = "This name is already taken. Choose a different username.",
                            color = MaterialTheme.colorScheme.error
                        )
                        vm.state.usernameAlert = false
                    } else {
                        if (vm.state.rUsername != "") {
                            navController.navigate("registerPassword/${vm.state.rUsername}")
                        }
                    }
                }

                Spacer(modifier = Modifier.size(10.dp))

                Button(onClick = {
                    vm.submitName()
                }) {
                    Text(
                        "next",
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        }
    }
}