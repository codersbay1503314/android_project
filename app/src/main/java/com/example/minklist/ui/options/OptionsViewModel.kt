package com.example.minklist.ui.options

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.minklist.data.local.clearTable
import com.example.minklist.data.local.schema.Shoppinglists
import com.example.minklist.data.local.schema.Users
import com.google.firebase.Firebase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.firestore
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.security.MessageDigest

class OptionsViewModel : ViewModel() {
    var state by mutableStateOf(OptionsState())
        private set

    private val dbRemote: FirebaseFirestore = Firebase.firestore

    // username input in login
    fun writeUsername(input: String) {
        state = state.copy(nameField = input)
    }

    // password input in login
    fun writePassword(input: String) {
        state = state.copy(passwordField = input)
    }

    // username input in useroptions
    fun changeUsername(input: String) {
        state = state.copy(nameChangeField = input)
    }

    // password hashing
    @OptIn(ExperimentalStdlibApi::class)
    fun hashMd5(str: String): String {
        return MessageDigest.getInstance("MD5").digest(str.toByteArray()).toHexString()
    }

    // login
    fun tryToLogin(
        name: String = state.nameField,
        password: String = state.passwordField
    ) {
        val user = dbRemote.collection("user").document(name)
        user.get()
            .addOnSuccessListener { document ->
                if (document.data != null) {
                    val username = document["name"] as String
                    val userpassword = document["password"] as String
                    val lists = document["lists"] as List<*>
                    foundUser(name, hashMd5(password), username, userpassword, lists)
                } else {
                    failedLogin()
                }
            }
            .addOnFailureListener {
                failedLogin()
            }
    }



    private fun foundUser(
        name: String,
        password: String,
        username: String,
        userpassword: String,
        lists: List<*>
    ) {
        if (name == username && password == userpassword) {
            succeedLogin(name, password, lists)
            state.loginAlert = false
            state.loginSuccess = true
        } else {
            failedLogin()
        }
    }

    private fun failedLogin() {
        state.loginAlert = true
        state.loginSuccess = false
        writeUsername("")
        writePassword("")
    }

    private fun succeedLogin(username: String, userpassword: String, lists: List<*>) {
        // put logged in user into local db
        transaction {
            Users.update({ Users.id eq 1 }) {
                it[name] = username
                it[password] = userpassword
            }
        }
        // put all listcodes into a List<String> in OptionsState
        for (code in lists) {
            state.listCodes += code.toString()
        }
        // go through listcodes: List<String> and add them to the local db
        for (code in state.listCodes) {
            getAllInfoFromList(db = dbRemote, code = code)
        }

        writeUsername("")
    }

    // ------- registration -------
    // Username
    fun writeRUsername(input: String) {
        if (input.length < 20)
            state = state.copy(rNameField = input)
    }

    fun submitName() {
        if (state.rNameField.length in 4..15) {
            isNameTaken(state.rNameField)
        }
    }

    private fun isNameTaken(name: String) {
        val user = dbRemote.collection("user").document(name)
        user.get()
            .addOnSuccessListener { document ->
                if (document.data != null) {
                    state.usernameAlert = true
                    writeRUsername("")
                } else {
                    state.usernameAlert = false
                    setRName()
                    writeRUsername("")
                }
            }
    }

    // Userpassword
    fun writeRPassword1(input: String) {
        state = state.copy(rPassword1Field = input)

    }

    fun writeRPassword2(input: String) {
        state = state.copy(rPassword2Field = input)
    }

    fun submitPassword(username: String, dbRemote: FirebaseFirestore) {
        if (state.rPassword1Field.length < 8) {
            state.passwordAlert1 = true
            writeRPassword2("")
        } else {
            state.passwordAlert1 = false
        }
        if (state.rPassword1Field != state.rPassword2Field) {
            state.passwordAlert2 = true
            writeRPassword2("")
        } else {
            state.passwordAlert2 = false
        }
        if (!state.passwordAlert1 && !state.passwordAlert2) {
            state.rPassword = state.rPassword1Field
            register(dbRemote)
        }
    }

    private fun setRName() {
        state.rUsername = state.rNameField
    }

    private fun register(db: FirebaseFirestore) {

        data class User(
            val name: String,
            val password: String,
            val lists: List<String>
        )

        val user = db.collection("user")
        user.document(state.rUsername)
            .set(
                User(
                    name = state.rUsername,
                    password = hashMd5(state.rPassword),
                    lists = listOf()
                )
            )
            .addOnFailureListener { e ->
                println("Minki traurig weil $e")
            }
        // put logged in user into local db
        clearTable()
        transaction {
            Users.update({ Users.id eq 1 }) {
                it[name] = state.rUsername
                it[password] = state.rPassword
            }
        }
    }

    fun logout() {
        transaction {
            Users.update({ Users.id eq 1 }) {
                it[name] = ""
                it[password] = ""
            }
        }
        clearTable()
        state.listCodes = mutableListOf()
    }

    fun changeUserName() {
        // TODO:  change Username
    }

    fun changePassword() {
        // TODO:  change PAssword
    }

    // list

    fun writeListName(input: String) {
        state = state.copy(listNameField = input)
    }

    fun changeListName() {
        // TODO: inplement changeListname
    }

    private fun getAllInfoFromList(db: FirebaseFirestore, code: String) {
        val list = db.collection("shoppinglists").document(code)
        list.get()
            .addOnSuccessListener { document ->
                val listname: String = document["name"] as String
                val items: List<String> = document["items"] as List<String>
                val user: List<String> = document["user"] as List<String>

                addListToLocalDB(code, listname, items, user)
            }
            .addOnFailureListener { e ->
                println("Minki traurig weil $e")
            }
    }

    private fun addListToLocalDB(
        code: String,
        name: String,
        items: List<String>,
        user: List<String>
    ) {
        transaction {
            Shoppinglists.insert {
                it[listCode] = code
                it[listName] = name
                it[listItems] = items
                it[listUser] = user
            }
        }
    }

}