package com.example.minklist.ui.options

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.outlined.Edit
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.minklist.R
import com.example.minklist.data.local.getUsername
import com.example.minklist.ui.drawer.TopBarView
import kotlinx.coroutines.launch

@Composable
fun UserOptionsScreen(
    vm: OptionsViewModel,
    navigationIconOnClick: () -> Unit,
    navController: NavController
) {
    TopBarView(
        title = "user options",
        navigationIconOnClick = navigationIconOnClick,
        actions = {}) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(20.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {

                Text(text = getUsername(), fontSize = 28.sp)

                IconButton(
                    onClick = {
                        navController.navigate(route = "emptyImplementation/drawer")
                    }) {
                    Icon(
                        imageVector = Icons.Outlined.Edit,
                        contentDescription = "edid profile",
                        modifier = Modifier.size(20.dp)
                    )
                }
            }

            Spacer(modifier = Modifier.size(50.dp))

            Button(onClick = {
                vm.logout()
                navController.navigate("login")
            }) {
                Text("logout")
            }
        }
    }
}