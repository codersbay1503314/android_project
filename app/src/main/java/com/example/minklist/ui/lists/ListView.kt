package com.example.minklist.ui.lists

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun ListScreen(
    db: FirebaseFirestore,
    vm: ListViewModel,
    listCode: String,
    innerPadding: PaddingValues,
    navController: NavController
) {

    Column(
        modifier = Modifier
            .padding(innerPadding)
            .padding(20.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Box(
            modifier = Modifier
                .size(width = 350.dp, height = 80.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.item,
                    placeholder = { Text("new item") },
                    onValueChange = { vm.writeItem(it) },
                    singleLine = true,
                    keyboardActions = KeyboardActions(
                        onDone = {
                            vm.addItem(listCode, db)
                            navController.navigate("list/$listCode")
                        }
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(15.dp),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {

                IconButton(onClick = {
                    vm.addItem(listCode, db)
                    navController.navigate("list/$listCode")
                }) {
                    Icon(
                        imageVector = Icons.Filled.AddCircle,
                        contentDescription = "Add item to list",
                        Modifier.size(35.dp)
                    )
                }
            }
        }


        // List Table
        LazyColumn {
            items(vm.getListItems(listCode).size) { index ->
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(80.dp)
                        .padding(10.dp)
                        .clip(RoundedCornerShape(20.dp))
                        .background(Color(0x50FFFFFF))
                        .border(1.dp, Color.Black, RoundedCornerShape(20.dp))
                        .padding(15.dp, 10.dp, 15.dp, 10.dp)
                ) {

                    Row(
                        modifier = Modifier
                            .fillMaxSize()
                            .clickable {
                                vm.checkItem(listCode, index, db)
                                navController.navigate("list/$listCode")
                            },
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Spacer(Modifier.size(5.dp))

                        if (vm.strikeThrough(vm.getListItems(listCode)[index])){
                            Text(
                                text = vm.getListItems(listCode)[index].dropLast(7),
                                fontSize = 18.sp,
                                textDecoration = TextDecoration.LineThrough
                            )
                        } else{
                            Text(
                                text = vm.getListItems(listCode)[index],
                                fontSize = 18.sp
                            )
                        }
                }
            }
        }
    }
}
}