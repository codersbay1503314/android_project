package com.example.minklist.ui.lists

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun CreateListView(
    db: FirebaseFirestore,
    vm: ListViewModel,
    navController: NavController,
    navAppController: NavHostController
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primaryContainer)
            .padding(20.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Column(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.background, RoundedCornerShape(25.dp))
                .padding(20.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text(
                text = "create new list",
                fontSize = 25.sp,
                color = MaterialTheme.colorScheme.primary
            )

            Column {
                Text(text = "list name", color = MaterialTheme.colorScheme.primary)
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.nameField,
                    onValueChange = { vm.writeListName(it) },
                    placeholder = { Text(text = "weekend grocery list \uD83C\uDF7D") },
                    singleLine = true
                )
            }
            Column {
                Text(text = "4-digit list code", color = MaterialTheme.colorScheme.primary)
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.codeField,
                    onValueChange = { vm.writeListCode(it) },
                    placeholder = { Text(text = "1234") },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                )
            }

            if (vm.state.listCodeAlert){
                Text(
                    text = "This code is already being used.",
                    color = MaterialTheme.colorScheme.error
                )
            }
            if (vm.state.listCodeSuccess && !vm.state.listCodeAlert)
                navAppController.navigate("load")

            Button(onClick = {
                vm.isCodeAvailable(db)
            }) {
                Text(
                    "create",
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }
        }
    }
}