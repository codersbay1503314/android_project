package com.example.minklist.ui.drawer

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Add
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.DrawerState
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.minklist.data.local.getFirstList
import com.example.minklist.data.local.getUsername
import com.example.minklist.ui.lists.AddListView
import com.example.minklist.ui.lists.ListOptionView
import com.example.minklist.ui.lists.ListTopBar
import com.example.minklist.ui.lists.ListViewModel
import com.example.minklist.ui.options.EmptyImplementation
import com.example.minklist.ui.options.UserOptionsScreen
import com.example.minklist.ui.options.OptionsViewModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.jetbrains.exposed.sql.Database

@Composable
fun Drawer(
    vm: DrawerViewModel,
    appNavController: NavHostController,
    dbRemote: FirebaseFirestore,
    dbLocal: Database
) {
    val navController = rememberNavController()

    val remDrawerState = rememberDrawerState(initialValue = vm.state.drawerValue)
    val scope = rememberCoroutineScope()
    ModalNavigationDrawer(
        drawerState = remDrawerState,
        drawerContent = {
            ModalDrawerSheet {
                Row(
                    modifier =
                    Modifier
                        .fillMaxWidth()
                        .height(80.dp)
                        .padding(20.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = getUsername(), fontSize = 28.sp)


                    IconButton(
                        onClick = {
                            navController.navigate(route = "options")
                            scope.launch { remDrawerState.close() }
                        }) {
                        Icon(
                            imageVector = Icons.Outlined.Settings,
                            contentDescription = "user Settings",
                            modifier = Modifier.size(26.dp)
                        )
                    }


                }

                HorizontalDivider()

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp)
                        .padding(20.dp, 10.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text("shopping lists")
                    Row {

                        ElevatedButton(
                            onClick = {
                                navController.navigate(route = "addList")
                                scope.launch { remDrawerState.close() }
                            }) {
                            Icon(
                                imageVector = Icons.Outlined.Add,
                                contentDescription = "add new shopping list",
                                modifier = Modifier.size(18.dp)
                            )
                            Spacer(modifier = Modifier.width(8.dp))
                            Text(text = "add list")
                        }
                    }


                }
                HorizontalDivider()

                AllListsFromUser(
                    dbRemote,
                    dbLocal,
                    navController,
                    scope,
                    remDrawerState,
                    { scope.launch { remDrawerState.close() } },
                    vm
                )
            }
        }
    ) {
        NavHost(
            navController,
            startDestination = vm.getStartDestination()
        ) {

            composable(
                "list/{listCode}",
                arguments = listOf(navArgument("listCode") { defaultValue = getFirstList() })
            ) { backStackEntry ->
                ListTopBar(
                    db = dbRemote,
                    vm = DrawerViewModel(),
                    backStackEntry.arguments?.getString("listCode"),
                    navigationIconOnClick = {
                        scope.launch {
                            remDrawerState.open()
                        }
                    }, navController = navController
                )
            }
            composable(
                "listoptions/{listCode}",
                arguments = listOf(navArgument("listCode") { defaultValue = "0000" })
            ) { backStackEntry ->
                ListOptionView(
                    db = dbRemote,
                    vm = ListViewModel(),
                    backStackEntry.arguments?.getString("listCode"),
                    navController = navController,
                    appNavController = appNavController
                )
            }
            composable(route = "options") {
                UserOptionsScreen(
                    vm = OptionsViewModel(), navigationIconOnClick = {
                        scope.launch {
                            remDrawerState.open()
                        }
                    }, appNavController
                )
            }
            composable(route = "addList") {
                AddListView(
                    dbRemote,
                    navigationIconOnClick = {
                        scope.launch {
                            remDrawerState.open()
                        }
                    }, navAppController = appNavController
                )
            }
            composable(
                route = "emptyImplementation/{goBackTo}",
                arguments = listOf(navArgument("goBackTo") { defaultValue = "addList" })
            ) { backStackEntry ->
                EmptyImplementation(
                    navController,
                    backStackEntry.arguments?.getString("goBackTo")
                )
            }
        }
    }
}

@Composable
fun AllListsFromUser(
    dbRemote: FirebaseFirestore,
    dbLocal: Database,
    navController: NavController,
    scope: CoroutineScope,
    remDrawerState: DrawerState,
    closeDrawer: () -> Unit,
    vm: DrawerViewModel
) {

    var list by remember { mutableStateOf(emptyList<Any?>()) }

    if (getUsername() != "") {
        vm.getAllListsFromUser(dbRemote, getUsername(), callback = { list = it })

        LazyColumn {
            items(list) {

                NavigationDrawerItem(
                    label = { Text(vm.getListName(it.toString())) },
                    selected = false,
                    onClick = {
                        vm.updateList(dbRemote, dbLocal, it.toString())
                        scope.launch { remDrawerState.close() }
                        navController.navigate("list/$it")
                        vm.setCurlist(it.toString())
                    }
                )
            }
        }
    }
}