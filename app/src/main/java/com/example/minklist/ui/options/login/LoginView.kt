package com.example.minklist.ui.options.login

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.minklist.data.local.clearTable
import com.example.minklist.ui.options.OptionsViewModel

@Composable
fun Login(
    vm: OptionsViewModel,
    navController: NavHostController
) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primaryContainer),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .padding(10.dp)
                .background(MaterialTheme.colorScheme.background, RoundedCornerShape(25.dp))
                .padding(20.dp, 10.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "login",
                style = TextStyle(color = MaterialTheme.colorScheme.primary, fontSize = 30.sp)
            )

            Column(
                modifier = Modifier
                    .padding(0.dp, 15.dp, 0.dp, 0.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.nameField,
                    onValueChange = { vm.writeUsername(it) },
                    label = { Text("username") },
                    singleLine = true,
                    keyboardActions = KeyboardActions(
                        onDone = {
                            vm.changeUserName()
                        }
                    )
                )

                Spacer(modifier = Modifier.size(20.dp))

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.passwordField,
                    onValueChange = { vm.writePassword(it) },
                    label = { Text("password") },
                    visualTransformation = PasswordVisualTransformation(),
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    keyboardActions = KeyboardActions(
                        onDone = {
                            vm.changePassword()
                        }
                    )
                )
                Spacer(modifier = Modifier.size(10.dp))

                if (vm.state.loginAlert) {
                    Text(
                        text = "Incorrect username or password.",
                        color = MaterialTheme.colorScheme.error
                    )
                    Spacer(modifier = Modifier.size(10.dp))
                } else if(vm.state.loginSuccess)
                    navController.navigate("drawer")

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Button(onClick = {
                        clearTable()
                        vm.tryToLogin()
                    }) {
                        Text(
                            "login",
                            color = MaterialTheme.colorScheme.onPrimary
                        )
                    }

                    Text(
                        text = "forgot password?",
                        color = MaterialTheme.colorScheme.onSurface,
                        modifier = Modifier.clickable { navController.navigate("emptyImplementation/login") }
                    )
                }

                Spacer(modifier = Modifier.size(20.dp))
            }


            Column(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = "don't have an account?",
                        color = MaterialTheme.colorScheme.onSurface
                    )
                    TextButton(onClick = { navController.navigate("register") }) {
                        Text("register")
                    }
                }

            }


        }
    }


}