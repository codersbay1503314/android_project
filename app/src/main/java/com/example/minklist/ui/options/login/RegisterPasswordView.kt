package com.example.minklist.ui.options.login

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.minklist.data.local.clearTable
import com.example.minklist.ui.options.OptionsViewModel
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun RegisterPassword(
    vm: OptionsViewModel,
    navController: NavHostController,
    username: String?,
    dbRemote: FirebaseFirestore
) {
    vm.state.rUsername = username.toString()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primaryContainer),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .padding(10.dp)
                .background(MaterialTheme.colorScheme.background, RoundedCornerShape(25.dp))
                .padding(20.dp, 10.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "register",
                style = TextStyle(color = MaterialTheme.colorScheme.primary, fontSize = 30.sp)
            )

            Column(
                modifier = Modifier
                    .padding(0.dp, 15.dp, 0.dp, 0.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {


                Text(
                    text = "Hey, ${vm.state.rUsername}!",
                    color = MaterialTheme.colorScheme.primary,
                    fontSize = 20.sp
                )

                Spacer(modifier = Modifier.size(5.dp))

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.rPassword1Field,
                    onValueChange = { vm.writeRPassword1(it) },
                    label = { Text("password") },
                    visualTransformation = PasswordVisualTransformation(),
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
                )

                Spacer(modifier = Modifier.size(20.dp))

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = vm.state.rPassword2Field,
                    onValueChange = { vm.writeRPassword2(it) },
                    label = { Text("repeat password") },
                    visualTransformation = PasswordVisualTransformation(),
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
                )
                Spacer(modifier = Modifier.size(10.dp))

                if (vm.state.passwordAlert1) {
                    Text(
                        text = "The password must contain at leaset 8 characters.",
                        color = MaterialTheme.colorScheme.error
                    )
                    Spacer(modifier = Modifier.size(10.dp))
                } else if (vm.state.passwordAlert2) {
                    Text(
                        text = "Please repeat the password correctly.",
                        color = MaterialTheme.colorScheme.error
                    )
                    Spacer(modifier = Modifier.size(10.dp))
                }

                Button(onClick = {
                    vm.submitPassword(username.toString(), dbRemote)
                    if (!vm.state.passwordAlert1 && !vm.state.passwordAlert2)
                        navController.navigate("drawer")
                }) {
                    Text(
                        "create account",
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        }
    }
}