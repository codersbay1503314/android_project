package com.example.minklist.ui.lists

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.example.minklist.ui.drawer.DrawerViewModel
import com.example.minklist.ui.drawer.TopBarView
import com.google.firebase.firestore.FirebaseFirestore

@Composable
fun ListTopBar(db: FirebaseFirestore, vm: DrawerViewModel, listCode: String?, navigationIconOnClick: () -> Unit, navController: NavController) {

    TopBarView(
        title = (vm.getListName(listCode.toString())),
        navigationIconOnClick = navigationIconOnClick,
        actions = {
            IconButton(onClick = { navController.navigate("listoptions/$listCode") }) {
                Icon(
                    imageVector = Icons.Filled.Edit,
                    contentDescription = "Edit current list"
                )
            }
        }) { innerPadding ->
        ListScreen(db = db, vm = ListViewModel(), listCode = listCode.toString(), innerPadding = innerPadding, navController = navController)
    }
}