package com.example.minklist.ui.drawer

import androidx.compose.material3.DrawerValue
import com.example.minklist.data.local.schema.Shoppinglists
import com.example.minklist.data.remote.ShoppingList
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

data class DrawerState(
    val drawerValue: DrawerValue = DrawerValue.Closed,
    val curlist: String = transaction {
        Shoppinglists.selectAll().where { Shoppinglists.id eq 1 }.forEach {
            return@transaction it[Shoppinglists.listCode]
        }
    }.toString()
)